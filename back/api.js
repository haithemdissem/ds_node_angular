const studentsRouter = require('./routes/students')
app.use('/students', studentsRouter)

const supervisorsRouter = require('./routes/supervisors')
app.use('/supervisors', supervisorsRouter)

const certificationRouter = require('./routes/certifications')
app.use('/certifications', certificationRouter )

const classroomRouter = require('./routes/classrooms')
app.use('/classrooms', classroomRouter )

const userRouter = require('./routes/users')
app.use('/users', userRouter )

const authRouter = require('./routes/auth')
app.use('/', authRouter )

module.exports = api