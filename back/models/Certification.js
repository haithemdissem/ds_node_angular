const mongoose = require('mongoose')

const CertificationSchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    time: {
        type: Number,
        min: 1,
        max:72,
        required: true,
    },
    description: String,
    created_at: {
        type: Date,
        default: Date.now(),
        required: true,
    },
})


module.exports = mongoose.model('Certifications', CertificationSchema)