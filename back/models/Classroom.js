const mongoose = require('mongoose')

const ClassroomSchema = mongoose.Schema({
    number: {
        type: Number,
        min: 1,
        required: true,
    },
    floor: Number,
    disponibilities: { 
        normal: [{
            day: { type: Number, min: 0 , max : 6 },
            start_time: { type: Number, min:8, max:18},
            end_time :{ type: Number, min:9, max:21},
        }],
        specifique: [{
            date: { type: Date },
            start_time: { type: Number, min:8, max:18},
            end_time :{ type: Number, min:9, max:21},
        }]    
    },
    created_at: {
        type: Date,
        default: Date.now(),
        required: true,
    },
})


module.exports = mongoose.model('Classrooms', ClassroomSchema)