const mongoose = require('mongoose'), Schema = mongoose.Schema
const User = require("./User");
const Certification = require("./Certification");

const StudentSchema = mongoose.Schema({

    _user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    _email: String,
    
    white_tests: [{
        _certification: {
            _id:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Certification'        
            },
            _certification_title: String,
        },
        test_at: Date
    }]
})

module.exports = mongoose.model('Students', StudentSchema)