const mongoose = require('mongoose')
const User = require("./User");

const SupervisorSchema = mongoose.Schema({

    _user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    _email: String,
    
    disponibilities: { 
        normal: [{
            day: { type: Number, min: 0 , max : 6 },
            start_time: { type: Number, min:8, max:18},
            end_time :{ type: Number, min:9, max:21},
        }],
        specific: [{
            date: { type: Date },
            start_time: { type: Number, min:8, max:18},
            end_time :{ type: Number, min:9, max:21},
        }]    
    },

})


module.exports = mongoose.model('Supervisors', SupervisorSchema)