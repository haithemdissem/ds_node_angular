const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Student = require('../models/Student');
const jwt = require('jsonwebtoken');


router.post('/register', async (req, res)=>{
    try{
        const user = await  new User({
            'first_name' : req.body.first_name,
            'last_name' : req.body.last_name,
            'email' : req.body.email,
            'role' : 'student',
            'password' : req.body.password,
        }).save()

        const student = await  new Student({
            '_user': user._id,
            '_email': user.email
        }).save()

        res.json({user, student})

    }catch(err){
        res.json({ message : err})
    }
    res.send(req.body)
})  
function authenticateToken(req,res,next){
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=> {
        if (err) return res.sendStatus(403)
        req.user = user;
        next();
    })
}


router.post('/login', async (req, res)=>{

    const email = req.body.email;
    const user = {  email: email }

    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
    res.send({accessToken : accessToken})

    /*
    try {
        const loggingUser = await User.find(
            {
                'email' : req.body.email,
                'password' : req.body.password
            }
        )

        if (_.isEmpty(loggingUser))
            res.json("user not found ")
        else
            res.json(loggingUser)
    }catch(err){
        res.json({message : err})
    }
    */

    /*
    const email = req.body.email
    const user = {email : email}
    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
    res.send({accessToken : accessToken})
    */
})  

module.exports = router