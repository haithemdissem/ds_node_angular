const express = require('express')
const router = express.Router()
const Certification = require('../models/Certification')

// Get all certifications
router.get('/', async (req,res)=>{
    try{
        res.json(await Certification.find())

    }catch(err){
        res.json({message : err})
    }
})
// Get specific certification
router.get('/:certification', async (req,res)=>{
    try {
        const specificCertif = await Certification.findById(req.params.certification)
        res.json(specificCertif)
    }catch(err){
        res.json({message : err})
    }
})

//Save cetification
router.post('/', async (req,res)=>{
    try{
        const certification = await new Certification({
            title : req.body.title,
            time : req.body.time,
            description : req.body.description
        }).save()
        res.json(certification)      
    }catch(err){
        res.json({ message : err})
    }

})

// Delete certification
router.delete('/:certification', async (req,res)=>{
    try {
        const removedCertif = await Certification.remove({_id : req.params.certification})
        res.json(removedCertif)
    }catch(err){
        res.json({message : err})
    }
})

// Update certification
router.patch('/:certification', async (req,res)=>{
   try {
        const updatedCertif = await Certification.updateMany(
            {_id : req.params.certification },
            { 
                title : req.body.title,
                time : req.body.time,
                time : req.body.time,
                description : req.body.description
            } 
        )
        res.json(updatedCertif)
    }catch(err){
        res.json({message : err})
    }
})

module.exports = router