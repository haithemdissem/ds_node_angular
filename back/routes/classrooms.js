const express = require('express')
const router = express.Router()
const Classroom = require('../models/Classroom')

// Get all classrooms
router.get('/', async (req,res)=>{
    try{
        const allclassrooms = await Classroom.find()
        res.json({allclassrooms})

    }catch(err){
        res.json({message : err})
    }
})
// Get specific classroom
router.get('/:classroom', async (req,res)=>{
    try {
        const specificClassroom = await Classroom.findById(req.params.classroom)
        res.json(specificClassroom)
    }catch(err){
        res.json({message : err})
    }
})

//Save classroom
router.post('/', async (req,res)=>{
    try{
        let normal=[]
        for(i=0 ; i<req.body.disponibilities.normal.length;i++)
        {
            normal.push({
                "day": req.body.disponibilities.normal[i].day,
                "start_time": req.body.disponibilities.normal[i].start_time,
                "end_time": req.body.disponibilities.normal[i].end_time,
            })
        }
        let specifique=[]
        for(i=0 ; i<req.body.disponibilities.specifique.length;i++)
        {
            specifique.push({
                "date": req.body.disponibilities.specifique[i].day,
                "start_time": req.body.disponibilities.specifique[i].start_time,
                "end_time": req.body.disponibilities.specifique[i].end_time,
            })
        }
        const classroom = await new Classroom({
            'number' : req.body.number,
            'floor' : req.body.floor,
            'disponibilities' : {
                'normal': normal,
                'specifique': specifique
            }
        }).save()    

        res.json({specificClassroom})      
    }catch(err){
        res.json({ message : err})
    }

})

// Delete classroom
router.delete('/:classroom', async (req,res)=>{
    try {
        const removedClassroom = await Classroom.remove({_id : req.params.classroom})
        res.json(removedClassroom)
    }catch(err){
        res.json({message : err})
    }
})

// Update classroom
router.patch('/:classroom', async (req,res)=>{
   try {
        const updatedClassroom = await Classroom.updateMany(
            {_id : req.params.classroom },
            { 
                'number' : req.body.number,
                'floor' : req.body.floor
            } 
        )
        res.json(updatedClassroom)
    }catch(err){
        res.json({message : err})
    }
})

module.exports = router

