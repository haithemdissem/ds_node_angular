const express = require('express')
const router = express.Router()

const Student = require('./../models/Student')
const User = require('./../models/User')
const Certification = require('../models/Certification')
const { updateOne } = require('./../models/User')

//Get all  white test of specific student
router.get('/:student', async (req,res)=>{
    try{
        let getStudent = await Student.findOne({'_user' : req.params.student})//edit for connected user
        res.json(getStudent)
    }catch(err){
        res.json({message : err})
    }
})


//Get all  white test of specific student
router.get('/', async (req,res)=>{
    try{
        let allStudents = await User.find({'role': 'student'})
        res.json(allStudents)
    }catch(err){
        res.json({message : err})
    }
})

//Add new  white test of specific student
router.post('/', async (req,res)=>{
    try{
        let certif = await Certification.findById(req.body.certification)
        let addWhiteTest = await Student.findOneAndUpdate(
            { '_user': req.body.user }, //edit for connected user
            {
                $push:{
                    'white_tests' : {
                        '_certification': {
                            '_id': certif._id,
                            '_certification_title': certif.title,
                        },
                        'test_at': Date.parse(req.body.test_at)
                    }
                }   
            },
        )
        res.json(addWhiteTest)
    }catch(err){
        res.json({message : err})
    }
})

//Update date of specific white test
router.patch('/:whiteTest', async (req,res)=>{
    try {
        let updateWhiteTest = await Student.updateOne(
            {'white_tests._id': req.params.whiteTest},
            {
                $set: {
                    'white_tests.$.test_at': Date.parse(req.body.test_at)
                }
            }
        )
        res.json(updateWhiteTest)
    }catch(err){
        res.json({message : err})
    }
})


//Delete specific white test
router.delete('/:whiteTest', async (req,res)=>{
    try {
        let deleteWhiteTest = await Student.findOneAndUpdate(
            {'_user': '5ee165987df2863ffccb513d'},//edit for connected user
            {
                $pull:{
                    'white_tests':{'_id': req.params.whiteTest}
                } 
            }
        )
        res.json(deleteWhiteTest)
    }catch(err){
        res.json({message : err})
    }
})

module.exports = router