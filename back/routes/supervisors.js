const express = require("express");
const router = express.Router();

const Supervisor = require("./../models/Supervisor");
const User = require("./../models/User");


//Get all supervisors
router.get("/", async (req, res) => {
  try {
    let allSupervisors = await User.find({ role: "supervisor" });
    res.json(allSupervisors);
  } catch (err) {
    res.json({ message: err });
  }
});


//Get specific supervisors
router.get("/:supervisor", async (req, res) => {
  try {
    let getSpecificSupervisor = await Supervisor.findOne({
      _user: req.params.supervisor,
    });
    res.json(getSpecificSupervisor);
  } catch (err) {
    res.json({ message: err });
  }
});


//Add supervisor
router.post("/", async (req, res) => {
  try {
    let user = await new User({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      role: "supervisor",
      password: req.body.password,
    }).save();

    let supervisor = await new Supervisor({
      _user: user._id,
      _email: user.email,
    }).save();

    res.json({ supervisor, user });
  } catch (err) {
    res.json({ message: err });
  }
});


//Get all normal disponibilities of specific supervisors
router.get("/:supervisor/normal", async (req, res) => {
  try {
    let getNormalDisponibilities = await Supervisor.findOne({
      _user: req.params.supervisor,
    });
    res.json(getNormalDisponibilities['disponibilities']['normal']);
  } catch (err) {
    res.json({ message: err });
  }
});
//Get all normal disponibilities of specific supervisors
router.get("/:supervisor/specific", async (req, res) => {
  try {
    let getSpecificDisponibilities = await Supervisor.findOne({
      _user: req.params.supervisor,
    });
    res.json(getSpecificDisponibilities['disponibilities']['specific']);
  } catch (err) {
    res.json({ message: err });
  }
});


//Add normal disponibility
router.post("/:supervisor/normal", async (req, res) => {
  try {
    let addNormalDisponibility = await Supervisor.findOneAndUpdate(
      { _user: req.params.supervisor }, //edit for connected user
      {
        $push: {
          "disponibilities.normal": {
            day: req.body.day,
            start_time: req.body.start_time,
            end_time: req.body.end_time,
          },
        },
      }
    );
    res.json(addNormalDisponibility);
  } catch (err) {
    res.json({ message: err });
  }
  console.log({
    day: req.body.day,
    start_time: req.body.start_time,
    end_time: req.body.end_time,
  })
});
//Add specific disponibility
router.post("/:supervisor/specific", async (req, res) => {
    try {
      let addSpecificDisponibility = await Supervisor.findOneAndUpdate(
        { _user: req.params.supervisor }, //edit for connected user
        {
          $push: {
            "disponibilities.specific": {
              date: Date.parse(req.body.date),
              start_time: req.body.start_time,
              end_time: req.body.end_time,
            },
          },
        }
      );
      res.json(addSpecificDisponibility);
    } catch (err) {
      res.json({ message: err });
    }
});


//Update normal disponibility
router.patch("/normal/:disponibiliy", async (req, res) => {
  try {
    let updateNormalDisponibilty = await Supervisor.updateOne(
      { "disponibilities.normal._id": req.params.disponibiliy },
      {
        $set: {
          "disponibilities.normal.$.day": req.body.day,
          "disponibilities.normal.$.start_time": req.body.start_time,
          "disponibilities.normal.$.end_time": req.body.end_time,
        },
      }
    );
    res.json(updateNormalDisponibilty);
  } catch (err) {
    res.json({ message: err });
  }
});
//Update specific disponibility
router.patch("/specific/:disponibiliy", async (req, res) => {
  try {
    let updateSpecificDisponibilty = await Supervisor.updateOne(
      { "disponibilities.specific._id": req.params.disponibiliy },
      {
        $set: {
          "disponibilities.specific.$.date": Date.parse(req.body.date),
          "disponibilities.specific.$.start_time": req.body.start_time,
          "disponibilities.specific.$.end_time": req.body.end_time,
        },
      }
    );
    res.json(updateSpecificDisponibilty);
  } catch (err) {
    res.json({ message: err });
  }
});


//Delete normal disponibiliy
router.delete("/normal/:disponibiliy", async (req, res) => {
  try {
    let deleteNormalDisponibiliy = await Supervisor.findOneAndUpdate(
      { _user: "5ee3e11fd4f70f3df093fb22" }, //edit for connected user
      {
        $pull: {
          "disponibilities.normal": { _id: req.params.disponibiliy },
        },
      }
    );
    res.json(deleteNormalDisponibiliy);
  } catch (err) {
    res.json({ message: err });
  }
});
//Delete specific disponibiliy
router.delete("/specific/:disponibiliy", async (req, res) => {
  try {
    let deleteSpecificDisponibiliy = await Supervisor.findOneAndUpdate(
      { _user: "5ee3e11fd4f70f3df093fb22" }, //edit for connected user
      {
        $pull: {
          "disponibilities.specific": { _id: req.params.disponibiliy },
        },
      }
    );
    res.json(deleteSpecificDisponibiliy);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
