const express = require('express')
const router = express.Router()

const User = require('../models/User')
const Supervisor = require('../models/Supervisor')

// Get all users
router.get('/', async (req,res)=>{
    try{
        let allusers = await User.find()
        res.json({allusers})
    }catch(err){
        res.json({message : err})
    }
})
// Get specific user
router.get('/:user', async (req,res)=>{
    try {
        let specificUser = await User.findById(req.params.user)
        res.json(specificUser)
    }catch(err){
        res.json({message : err})
    }
})

// Delete user
router.delete('/:user', async (req,res)=>{
    try {
        const removedUser = await User.remove({_id : req.params.user})
        res.json(removedUser)
    }catch(err){
        res.json({message : err})
    }
})

// Update user 
router.patch('/:user', async (req,res)=>{

   try {
        const updatedUser = await User.updateMany(
            {_id : req.params.user },
            { 
                'first_name' : req.body.first_name,
                'last_name' : req.body.last_name ,
                'email' : req.body.email,
                'role' : req.body.role,
                'password' : req.body.password
            } 
        )
        res.json(updatedUser)
    }catch(err){
        res.json({message : err})
    }
})

module.exports = router