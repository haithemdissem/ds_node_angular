require('dotenv').config()

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const  jwt = require('jsonwebtoken')
var _ = require('lodash');
var moment = require('moment'); 



const app = express()
//app.use(express.urlencoded({extended:false}))
app.use(bodyParser.json())


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PATCH,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
});

//Routes
const studentsRouter = require('./routes/students')
app.use('/students', studentsRouter)

const supervisorsRouter = require('./routes/supervisors')
app.use('/supervisors', supervisorsRouter)

const certificationRouter = require('./routes/certifications')
app.use('/certifications', certificationRouter )

const classroomRouter = require('./routes/classrooms')
app.use('/classrooms', classroomRouter )

const userRouter = require('./routes/users')
app.use('/users', userRouter )

const authRouter = require('./routes/auth')
app.use('/', authRouter )




//Connect to db
mongoose.connect(process.env.DB_CONNECTION,
    {
        useNewUrlParser: true, 
        useUnifiedTopology: true 
    },
    () => console.log("connect to db!")
)

//Server port
app.listen(3000)
